#!/usr/bin/env python3

"""dedup: a file deduplicator at the command-line.

First deduces the common parent folder of all the given folders and \
recursively gathers the list of all the files in them, *not* following \
symbolic links. If asked so, reads the files' size and content hash \
information from the cache. Then fetches the remaining files' size and \
computes their content hash information, reading data in chuncks and \
using as many cores as possible. Builds a list of groups of duplicate \
files and interactively prompts for which file to keep in each group, \
from the biggest to the smallest.
"""

__app__ = "dedup"
__version__ = "0.1.0"

import argparse
import collections
import hashlib
import itertools
import multiprocessing
import os
import pathlib
import pickle
import random
import sys
import time

CACHE_PATH = pathlib.Path.home().joinpath(".cache/dedup-python/cache.pickle")
UNITS = {
    1_000: ("octet(s)", "ko", "Mo", "Go", "To", "Po", "Eo", "Zo", "Yo"),
    1_024: ("octet(s)", "Kio", "Mio", "Gio", "Tio", "Pio", "Eio", "Zio", "Yio"),
}
TEMPERATURE_THRESHOLD = 60  # degrees Celcius
COOLDOWN_TIME = 0.2  # seconds
CHUNK_SIZE = 50_000_000  # octets
MINIMUM_SIZE = 0  # octets


def resolve_folders(folders):
    """Resolves folders and computes the closest parent."""

    # Warns about one of the given paths being a symbolic link.
    symlink_warning = False
    if any(folder.is_symlink() for folder in folders):
        print(
            # bold, yellow
            "\33[1;93mWARNING:",
            "at least one of the paths given is a symbolic link.\33[0m",  # reset
        )
        symlink_warning = True

    # Builds a clean list of folders.
    folder_list = sorted(set(folder.expanduser().resolve() for folder in folders))

    # Crashes right away if one of the given paths happens not to be a
    # folder.
    if not all(folder.is_dir() for folder in folder_list):
        print(
            # bold, red
            "\33[1;91mERROR:",
            "at least one of the paths given is not a directory.\33[0m",  # reset
        )
        sys.exit(1)

    # Finds the closest parent folder.
    closest_parent = pathlib.Path()
    for this_level in zip(
        *(
            tuple(pathlib.Path(part) for part in this_path.parts)
            for this_path in folder_list
        )
    ):
        if len(set(this_level)) == 1:
            closest_parent = closest_parent.joinpath(this_level[0])
        else:
            break

    return closest_parent, folder_list, symlink_warning


def load_cache():
    """Loads the cache from the pickle file."""
    try:
        with CACHE_PATH.open("rb") as pickle_file:
            return pickle.load(pickle_file)
    except (FileNotFoundError, EOFError):
        return {}


def dump_cache(cache):
    """Dumps the cache to the pickle file."""
    CACHE_PATH.parent.mkdir(parents=True, exist_ok=True)
    with CACHE_PATH.open("wb") as pickle_file:
        pickle.dump(cache, pickle_file, pickle.HIGHEST_PROTOCOL)


def update_cache(fresh_cache, purge):
    """Updates the cache in the pickle file."""
    if purge:
        cache = {
            this_path: this_info
            for this_path, this_info in load_cache().items()
            if this_path.is_file()
        }
    else:
        cache = load_cache()
    cache.update(fresh_cache)
    dump_cache(cache)


def get_temp():
    """Gets the CPU temperature in degrees Celcius."""
    zones_number = 0
    temp_reading = 0
    for this_folder in pathlib.Path("/sys/class/thermal").glob("*"):
        try:
            with this_folder.joinpath("temp").open("r") as temp_file:
                temp_reading += int(temp_file.read())
        except FileNotFoundError:
            pass
        else:
            zones_number += 1
    return temp_reading / 1_000 / zones_number


def get_hashes(this_path):
    """Gets the hashes of a file, reading content in chunks."""
    sha512_hash = hashlib.sha512()
    blake2_hash = hashlib.blake2b()
    with this_path.open("rb") as read_file:
        file_data = read_file.read(CHUNK_SIZE)
        while file_data:
            while get_temp() > TEMPERATURE_THRESHOLD:
                time.sleep(COOLDOWN_TIME)
            sha512_hash.update(file_data)
            blake2_hash.update(file_data)
            file_data = read_file.read(CHUNK_SIZE)
    return sha512_hash.hexdigest(), blake2_hash.hexdigest()


def get_info(this_path):
    """Gathers information about a file."""
    if this_path.is_symlink():
        return {}
    if not this_path.is_file():
        return {}
    size = this_path.stat().st_size
    if size <= MINIMUM_SIZE:
        return {}
    return {
        "key": (size, *get_hashes(this_path)),
        "path": this_path,
        "mtime": int(this_path.stat().st_mtime),
    }


def pretty_size(size, system=1_024):
    """Converts a number of octets to a common multiple.

    >>> pretty_size(0)
    '0 octet(s)'
    >>> pretty_size(0, system=1_000)
    '0 octet(s)'
    >>> pretty_size(999)
    '999 octet(s)'
    >>> pretty_size(999, system=1_000)
    '999 octet(s)'
    >>> pretty_size(1_000)
    '1,000 octet(s)'
    >>> pretty_size(1_000, system=1_000)
    '1.00 ko'
    >>> pretty_size(1_024)
    '1.00 Kio'
    >>> pretty_size(1_024, system=1_000)
    '1.02 ko'
    >>> pretty_size(68_532_880_032_826_397_206_378_982)
    '56.69 Yio'
    >>> pretty_size(68_532_880_032_826_397_206_378_982, system=1_000)
    '68.53 Yo'
    >>> pretty_size(32_486_275_295_313_951_786_284_111_282)
    '26,872.02 Yio'
    >>> pretty_size(32_486_275_295_313_951_786_284_111_282, system=1_000)
    '32,486.28 Yo'
    """
    iteration = 0
    while size >= system and iteration < (len(UNITS[system]) - 1):
        size = size / system
        iteration += 1
    if not iteration:
        prettier_size = f"{size:,d}"
    else:
        prettier_size = f"{size:,.2f}"
    return f"{prettier_size} {UNITS[system][iteration]}"


def summary(freed, total_duplicate_size, total_size, final=False, **kwargs):
    """Prints a summary."""
    if freed:
        first_line = ""
        if (not final) and kwargs["no_action"]:
            # bold, blue
            first_line = "\33[1;94mSo far, would have freed"
        elif (not final) and not kwargs["no_action"]:
            first_line = "\33[1;94mSo far, freed"
        elif final and kwargs["no_action"]:
            first_line = "\33[1;94mWould have freed"
        elif final and not kwargs["no_action"]:
            first_line = "\33[1;94mFreed"
        print(
            first_line,
            f"{pretty_size(freed)}, {freed / total_duplicate_size:.2%} of duplicate",
            f"data, {freed / total_size:.2%} of total.",
            "\33[0m",  # reset
        )


def multiprocess_files(function, path_list):
    """Multiprocesses files with a progress indication."""
    result_list = []
    if path_list:
        paths_count = len(path_list)
        with multiprocessing.Pool() as pool:
            if paths_count <= os.cpu_count():
                result_list = pool.map(function, path_list)
            else:
                for number, path_result in enumerate(
                    pool.imap_unordered(function, path_list), start=1
                ):
                    sys.stdout.write(
                        f"\rChecked {number:,d} / {paths_count:,d} files "
                        f"({number / paths_count:.0%}). "
                        f"Processor temperature: {get_temp():.1f}°C."
                    )
                    result_list.append(path_result)
                print()
    return result_list


def double_check(path_list):
    """Double-checks that files are identical."""
    if (
        len(
            set(
                (this_path.stat().st_dev, this_path.stat().st_ino)
                for this_path in path_list
            )
        )
        == 1
    ):
        return True
    if len(set(this_path.stat().st_size for this_path in path_list)) != 1:
        return False
    hashes_list = multiprocess_files(get_hashes, path_list)
    if len(set(hashes_list)) != 1:
        return False
    return True


def print_cache(cache):
    """Pretty-prints a random sample of the cache."""

    def print_path_info(this_path, this_info):
        print(
            (
                f'{str(this_path)+",":<68}'
                if len(str(this_path)) < 70
                else f"{str(this_path)[:33]}…{str(this_path)[-33:]},"
            ),
            f'sha512: {this_info["key"][1][:5]}…{this_info["key"][1][-5:]},',
            f'blake2: {this_info["key"][2][:5]}…{this_info["key"][2][-5:]},',
            f'{pretty_size(this_info["key"][0])}',
        )

    sample_size = 20
    if len(cache) <= sample_size:
        for this_path, this_info in cache.items():
            print_path_info(this_path, this_info)
    else:
        threshold = sample_size / len(cache)
        for this_path, this_info in cache.items():
            if random.random() < threshold:
                print_path_info(this_path, this_info)


def main(**kwargs):
    """Main function."""

    closest_parent, folder_list, symlink_warning = resolve_folders(kwargs["folder"])
    if len(folder_list) > 1:
        print("Folders:", folder_list[0])
        for folder in folder_list[1:]:
            print("        ", folder)
        print("Common parent folder:", closest_parent)
    else:
        print("Folder:", folder_list[0])
    if MINIMUM_SIZE:
        print(f"Files smaller than {pretty_size(MINIMUM_SIZE)} are ignored.")
    else:
        print("All files - regardless of size - are considered.")
    if symlink_warning:
        if kwargs["shallow"]:
            print(
                "Loading the list of direct children files, *not* following symbolic links…"
            )
        else:
            print(
                "Recursively loading the list of files, *not* following symbolic links…"
            )
    else:
        if kwargs["shallow"]:
            print("Loading the list of direct children files…")
        else:
            print("Recursively loading the list of files…")
    if kwargs["shallow"]:
        folder_paths = set(
            itertools.chain(*(folder.glob("*") for folder in folder_list))
        )
    else:
        folder_paths = set(
            itertools.chain(*(folder.rglob("*") for folder in folder_list))
        )

    if kwargs["use_cache"]:
        print("Loading the cache…")
        cache = {
            this_path: this_info
            for this_path, this_info in load_cache().items()
            if this_path in folder_paths and this_info["key"][0] > MINIMUM_SIZE
        }
    else:
        cache = {}

    cache_size = sum(this_info["key"][0] for this_info in cache.values())
    if cache_size:
        print(
            "Spared checking",
            pretty_size(cache_size),
            f"of data in {len(cache):,d}",
            "files thanks to the cache.",
        )

    full_map = collections.defaultdict(set)
    for this_path, this_info in cache.items():
        full_map[this_info["key"]].add(this_path)

    new_paths = {
        this_path
        for this_path in folder_paths
        if this_path not in cache.keys()
        or cache[this_path]["mtime"] != int(this_path.stat().st_mtime)
    }

    if new_paths:
        print("Checking remaining files…")
    info_list = multiprocess_files(get_info, new_paths)

    fresh_size = sum(info["key"][0] for info in info_list if info)
    if fresh_size:
        print(
            "Freshly checked" if cache_size else "Checked",
            pretty_size(fresh_size),
            "of data.",
        )
    total_size = cache_size + fresh_size

    fresh_cache = {
        info["path"]: {"key": info["key"], "mtime": info["mtime"]}
        for info in info_list
        if info
    }
    if fresh_cache:
        if kwargs["reset_cache"]:
            print("Saving the files' information to fresh cache…")
            cache.update(fresh_cache)
            dump_cache(cache)
        else:
            print(
                "Updating the cache with the files'",
                (
                    "information, dropping information from any non-existing file…"
                    if kwargs["purge_cache"]
                    else "information…"
                ),
            )
            update_cache(fresh_cache, kwargs["purge_cache"])
    else:
        print(
            "Freshly checked" if cache_size else "Checked",
            (
                f"files are all smaller than {pretty_size(MINIMUM_SIZE)}."
                if MINIMUM_SIZE
                else "files are all empty."
            ),
        )

    for info in info_list:
        if info:
            full_map[info["key"]].add(info["path"])

    duplicate_map = {
        key: path_list for key, path_list in full_map.items() if len(path_list) > 1
    }
    if kwargs["match"]:
        this_match = kwargs["match"].lower()
        duplicate_map = {
            key: path_list
            for key, path_list in duplicate_map.items()
            if any(this_match in this_path.name.lower() for this_path in path_list)
        }
    if kwargs["different_folders"]:
        duplicate_map = {
            key: path_list
            for key, path_list in duplicate_map.items()
            if len(
                set(
                    pathlib.Path(this_path.relative_to(closest_parent).parts[0])
                    for this_path in path_list
                )
            )
            > 1
        }
    if not duplicate_map:
        print("No duplicates found.")
        sys.exit(0)
    total_duplicate_groups = len(duplicate_map)
    total_duplicate_files = sum(
        len(path_list) - 1 for key, path_list in duplicate_map.items()
    )
    total_duplicate_size = sum(
        key[0] * (len(path_list) - 1) for key, path_list in duplicate_map.items()
    )
    freed = 0
    print(
        # bold, blue
        "\33[1;94mFound",
        f"{pretty_size(total_duplicate_size)} of duplicate data",
        f"({total_duplicate_size / total_size:.2%} of total)",
        f"in {total_duplicate_files:,d}",
        "file" if total_duplicate_files == 1 else "files",
        f"spread across {total_duplicate_groups:,d}",
        "group." if total_duplicate_groups == 1 else "groups.",
        "\33[0m",  # reset
    )

    if kwargs["scan"]:
        sys.exit(0)

    for duplicate_number, duplicate_items in enumerate(
        sorted(
            duplicate_map.items(),
            key=lambda items: items[0][0] * (len(items[1]) - 1),
            reverse=True,
        ),
        start=1,
    ):
        path_list = duplicate_items[1]
        individual_size = duplicate_items[0][0]
        duplicate_size = individual_size * (len(path_list) - 1)
        if kwargs["sort_by_file_name"]:
            duplicates = {
                str(number): this_path
                for number, this_path in enumerate(
                    sorted(path_list, key=lambda item: (item.name, item.parent)),
                    start=1,
                )
            }
        else:
            duplicates = {
                str(number): this_path
                for number, this_path in enumerate(sorted(path_list), start=1)
            }
        print(
            # bold, green
            "\33[1;32mGroup",
            f"{duplicate_number:,d} / {total_duplicate_groups:,d}",
            f"({duplicate_number / total_duplicate_groups:.0%}).",
            f"Individual size: {pretty_size(individual_size)}.",
            f"Duplicate size: {pretty_size(duplicate_size)},",
            f"{duplicate_size / total_duplicate_size:.2%} of duplicate data,",
            f"{duplicate_size / total_size:.2%} of total.",
            "\33[0m",  # reset
        )
        if len(duplicates) >= 10 and len(duplicates) < 100:
            for number, this_path in duplicates.items():
                print(f"{number:>2}: {this_path.relative_to(closest_parent)}")
        else:
            for number, this_path in duplicates.items():
                print(f"{number}: {this_path.relative_to(closest_parent)}")
        duplicates_keys = sorted(duplicates.keys())
        domain = tuple((*duplicates_keys, "f", "l", "a"))
        answer = "1" if kwargs["no_action"] else ""
        while answer not in domain:
            try:
                answer = (
                    input(
                        f'Which one to keep? ({", ".join(domain[:-3])}, '
                        f"f to keep the first one: {duplicates_keys[0]}, "
                        f"l to keep the last one: {duplicates_keys[-1]}, "
                        "a to keep them all, CTRL+C to stop) "
                    )
                    .strip()
                    .lower()
                )
            except KeyboardInterrupt:
                print()
                summary(freed, total_duplicate_size, total_size, final=True, **kwargs)
                sys.exit(0)
        if answer == "f":
            answer = duplicates_keys[0]
        if answer == "l":
            answer = duplicates_keys[-1]
        if answer == "a":
            continue
        print("Double-checking…")
        try:
            check_ok = double_check(path_list)
        except FileNotFoundError:
            print(
                # bold, yellow
                "\33[1;93mWARNING:",
                "at least one of the files no longer exists. Skipping…"
                "\33[0m",  # reset
            )
            continue
        if not check_ok:
            print(
                # bold, yellow
                "\33[1;93mWARNING:",
                "those files are no longer all the same. Skipping…\33[0m",  # reset
            )
            continue
        for number, this_path in duplicates.items():
            if str(number) == str(answer):
                continue
            if not kwargs["no_action"]:
                try:
                    this_path.unlink()
                except FileNotFoundError:
                    print(
                        # bold, yellow
                        "\33[1;93mWARNING:",
                        f"file {number}:",
                        this_path.relative_to(closest_parent),
                        "no longer exists.\33[0m",  # reset
                    )
                    continue
            freed += individual_size
            print(
                # red
                "\33[31mWould delete" if kwargs["no_action"] else "\33[31mDeleted",
                f"{number}: {this_path.relative_to(closest_parent)}",
                "\33[0m",  # reset
            )
        summary(freed, total_duplicate_size, total_size, **kwargs)
    summary(freed, total_duplicate_size, total_size, final=True, **kwargs)


def cli():
    """Creates a parser to capture command-line arguments."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "folder", nargs="+", type=pathlib.Path, help="folder(s) to be analysed"
    )
    parser.add_argument(
        "-m",
        "--match",
        help="a string to match in the filename (case insensitive)",
    )
    parser.add_argument(
        "-a",
        "--shallow",
        action="store_true",
        help=(
            "only look for direct children files in each provided folder "
            "(no recursive search)"
        ),
    )
    parser.add_argument(
        "-d",
        "--different-folders",
        action="store_true",
        help=(
            'look for files in different "base" folders - that is, in different '
            "children folders just below the common parent folder"
        ),
    )
    parser.add_argument("-c", "--use-cache", action="store_true", help="use the cache")
    clean_cache = parser.add_mutually_exclusive_group()
    clean_cache.add_argument(
        "-r",
        "--reset-cache",
        action="store_true",
        help="drop the cache for all files outside this call",
    )
    clean_cache.add_argument(
        "-p",
        "--purge-cache",
        action="store_true",
        help="drop the cache only for files not found anymore - may take a long time",
    )
    parser.add_argument(
        "-f",
        "--sort-by-file-name",
        action="store_true",
        help="in each group of duplicate files, sort by file name instead of full path",
    )
    parser.add_argument(
        "-s",
        "--scan",
        action="store_true",
        help="analyse files, cache information, stop there",
    )
    parser.add_argument(
        "-n", "--no-action", action="store_true", help="simulation (dry run)"
    )
    return parser


if __name__ == "__main__":
    PARSER = cli()
    main(**vars(PARSER.parse_args()))
