#!/usr/bin/env python3

"""drop_cache_entry: drops specified files' information from the cache,
case-sensitively matching the file name only.
"""

import argparse

import dedup

__app__ = dedup.__app__ + "_drop_cache_entry"
__version__ = dedup.__version__


def main(**kwargs):
    """Main function."""
    old_cache_path = dedup.CACHE_PATH.with_name("old_cache.pickle")
    old_cache = dedup.load_cache()
    dedup.CACHE_PATH.rename(old_cache_path)
    new_cache = {
        this_path: this_info
        for this_path, this_info in old_cache.items()
        if this_path.name not in kwargs["file"]
    }
    dedup.dump_cache(new_cache)


def cli():
    """Creates a parser to capture command-line arguments."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("file", nargs="+", help="name(s) of the file(s) to be dropped")
    return parser


if __name__ == "__main__":
    PARSER = cli()
    main(**vars(PARSER.parse_args()))
