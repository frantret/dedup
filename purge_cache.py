#!/usr/bin/env python3

"""purge_cache: drops non-existing files' information from the cache."""

import argparse

import dedup

__app__ = dedup.__app__ + "_purge_cache"
__version__ = dedup.__version__


def main():
    """Main function."""
    old_cache_path = dedup.CACHE_PATH.with_name("old_cache.pickle")
    old_cache = dedup.load_cache()
    dedup.CACHE_PATH.rename(old_cache_path)
    new_cache = {
        this_path: this_info
        for this_path, this_info in old_cache.items()
        if this_path.exists()
    }
    dedup.dump_cache(new_cache)


def cli():
    """Creates a parser to capture command-line arguments."""
    parser = argparse.ArgumentParser(description=__doc__)
    return parser


if __name__ == "__main__":
    cli()
    main()
