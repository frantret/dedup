# Changelog

## 0.1.0: First useful version (14 May 2024)

-   New
    -   Duplicates identification by content: SHA-512 and BLAKE2b hashes, plus
        size.
    -   Cache file information in a pickle file in the user directory
        (`$HOME/.cache/dedup-python/cache.pickle`).
    -   Options to:
        -   match files by name
        -   do a shallow search instead of a recursive one
        -   flag duplicates in different folders
        -   sort files by name instead of full path
        -   scan only
        -   simulation
