# dedup: a file deduplicator at the command-line

## Dependencies

It is implemented in Python. Its only dependency is a basic Python
installation. Tested on Python 3.11 and Linux.

## Features

-   duplicates identification by content: SHA-512 and BLAKE2b hashes, plus size
-   cache file information in a pickle file in the user directory
    (`$HOME/.cache/dedup-python/cache.pickle`)
-   options to:
    -   match files by name
    -   do a shallow search instead of a recursive one
    -   flag duplicates in different folders
    -   sort files by name instead of full path
    -   scan only
    -   simulation

## Use

Command line interface:

```bash
./dedup.py <folder1> <folder2>
```

Description of the options:

```bash
./dedup.py -h
```

## License

[3-clause BSD-type](LICENSE.md).
